[![pipeline status](https://gitlab.com/civiup/survey/badges/master/pipeline.svg)](https://gitlab.com/civiup/survey/commits/master) [![coverage report](https://gitlab.com/civiup/survey/badges/master/coverage.svg)](https://gitlab.com/civiup/survey/commits/master) [![codecov](https://codecov.io/gl/civiup/survey/branch/master/graph/badge.svg)](https://codecov.io/gl/civiup/survey)  

## Why and what we do. How do we understand customer needs and how/if you use the service

1. [Questionnaire](https://webapp-7egl.onrender.com/questionnaire.html) based on [theory](https://en.wikipedia.org/wiki/Kano_model)
1. We use Clicky free plan. The price depends on segments, heatmap and API throttling

## How do we prevent security incidents

1. Dependency scanning with `yarn audit` and with snyk
1. Active scanning with zap and arachni
1. [Detect insecure fuction calls](https://snyk.io/docs/runtime-protection/)

## How do we detect and respond to security incidents

1. [Respond to common attacks](https://my.sqreen.com)

## How can we improve our security

1. Write how we accept concerns/issues related to security
1. [Static Application Security Test](https://github.com/ajinabraham/NodeJsScan)
1. Active scanning with openvas or w3af

## What can we pay to improve our security 

1. Buy a cheap CI service like [this](https://portswigger.net/burp/pro) or [this](https://hackertarget.com/scan-membership)
1. Hackerone

## When adding a dependency,

1. check if you really need it
1. check if you can develop it, some npm packages are tiny and outdated
1. detect protective licenses with npm-consider

