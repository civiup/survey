import { Application, Controller } from "stimulus";
import ExampleController from "example_controller.js";


describe("ExampleController", () => {
  describe("#connect", () => {
    beforeEach(() => {
      document.body.innerHTML = `<div id="element" data-controller="example">
      </div>`;

      const application = Application.start();
      application.register("example", ExampleController);
    });

    it("has the word works", () => {
      const output = document.getElementById("element");

      expect(output.innerHTML).toContain("works");
    });
  });
});
