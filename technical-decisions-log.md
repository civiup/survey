### Technical decisions

Value: Customer feedback over assumptions: We always look to make the quickest change possible that improves the user's outcome to get feedback.
WoW: Simplicity: Thinking about how big we can possibly get, leads to a whole bunch of things that probably don’t matter right now, and it’s going to make things more complicated, harder to change later and it’s gonna take longer. So now we do things that don't scale.

phase #1: Jotform

phase #2: HTML

Problem = Jotform cannot have a favicon, so we had to embed it to our HTML.

Decision = render.com allows to work for now with no need to setup a anything, just put your HTML, Gemfile, package.json and it serves it and it can hold a postgres if necessary.

Side effect = Through open graph meta HTML tags we can now control the thumbnail in social media

phase #3: jekyll

jekyll was an easy way to create a landing page based on bootstrap agency theme while focusing on the content, neither on HTML, nor on CSS or JS.

phase #4: nodejs

Problem = We cannot have free Visitor analytics with ahoy and free basic behavioral analytics with blazer in jekyll but we could have that in nodejs. Also we want to have content in markdown files.

Decision = Replace jekyll with expressJS, stimulusJS (webpack is here to take care of ES6 and SCSS compilation) and markedJS demoed at gitlab pages and [fantom](https://marketplace.digitalocean.com/apps/fathom-analytics)
Old links: [[1](https://blog.capsens.eu/stimulusjs-on-rails-101-f22a0818dff8)] Then add ahoy like and blazer like but in nodejs. [[2](https://gorails.com/episodes/internal-metrics-with-ahoy-and-blazer)]

