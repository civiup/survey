Internal (The Stuff we create)

External (The Stuff we use but we cannot change)

Research (Not actively used, but interesting anyway)

## Local environment

Use a *nix based OS.

```
gem install license_finder
```

When adding a dependency, run locally the following before you `git push`:

1. `snyk`. Snyk will alert us when there is a version of a dependency that needs update due to a vulnerability.

## Variables

At [CI](https://gitlab.com/civiup/survey/-/settings/ci_cd): `CODECOV_TOKEN`

At render.com env group:
```
NODE_ENV
SQREEN_APP_NAME
SQREEN_TOKEN
SNYK_PROJECT_ID
```

## To update the security scanner

```
git clone git@gitlab.com:civiup/ScanJs4Security.git
# copy content from https://github.com/ajinabraham/NodeJsScan
docker login registry.gitlab.com
docker build -t registry.gitlab.com/civiup/scanjs4security -f cli.dockerfile .
# apply again these changes https://gitlab.com/civiup/ScanJs4Security/commit/60ceaf872c71c2bbe6ef9703f1a2c8b789b50209
docker push registry.gitlab.com/civiup/scanjs4security
```

## Dependencies

```mermaid
graph TB;
  subgraph which-security-configuration-or-dependency-is-missing;
    user-->sqreen;
    sqreen-->server-at-render.com;
  end;
  subgraph which-code-is-insecure;
    gitlab-ci-->gitlab-registry;
    gitlab-registry-->NodeJsScan;
  end;
  subgraph which-campaign-brings-more-visitors;
    user-->clicky;
    clicky-->server-at-render.com;
    server-at-render.com-->clicky;
  end;
  subgraph which-HR-is-the-biggest-need-identified;
    user-->jotform;
    jotform-->GoogleSheets;
    GoogleSheets-->insight;
  end;
```

